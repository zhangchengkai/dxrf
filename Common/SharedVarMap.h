#pragma once
#include <unordered_map>
#include <DirectXMath.h>
#include <string>
#include <vector>
#include <algorithm>
#include <Windows.h>


class SharedVarMap
{
	using Byte = unsigned char;

	static constexpr size_t MAX_DATA_SIZE = 64;

public:
	SharedVarMap(std::string name, bool is_creator);
	~SharedVarMap();

	template<typename T>
	T get(const std::string &name, const T &type_notifier)
	{
		WaitForSingleObject(_h_shared_mem_mutex, INFINITE);
		T val = *(reinterpret_cast<T *>((*_p_map)[name].data)); 
		ReleaseMutex(_h_shared_mem_mutex);
		return val;
	}

	template<typename T>
	void set(const std::string &name, const T &val)
	{
		WaitForSingleObject(_h_shared_mem_mutex, INFINITE);
		*(reinterpret_cast<T *>((*_p_map)[name].data)) = val;
		ReleaseMutex(_h_shared_mem_mutex);
	}

	struct Value
	{
		Byte data[MAX_DATA_SIZE];
	};
	static constexpr DWORD MAX_MAP_SIZE = 0xfffffful; // 16,777,215 B ~ 16 MB
	std::unordered_map<std::string, Value> *_p_map;
private:
	bool _b_is_creator;

	HANDLE _h_shared_mem_file;
	HANDLE _h_shared_mem_obj;
	HANDLE _h_shared_mem_mutex;
};

