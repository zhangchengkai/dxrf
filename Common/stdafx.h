#pragma once
#include <Windows.h>
#include <crtdbg.h>
#include <d3d12.h>
#include <wrl.h>
#include <dxgi1_4.h>
#include "d3dx12.h"
#include <DirectXMath.h>
#include <string>
#include <fstream>
#include <d3dcompiler.h>
#include <vector>
#include <array>
#include <DirectXColors.h>
#include <unordered_map>
#include <DirectXCollision.h>
#include <chrono>
#include <thread>


#ifdef _DEBUG
#ifdef ASSERT
#undef ASSERT
#endif
#define ASSERT(x) _ASSERTE(x)
#else
#define ASSERT(x) (x)
#endif


#ifdef _DEBUG
#ifdef SUCCEED
#undef SUCCEED
#endif
#define SUCCEED(x) SUCCEEDED(x)
#else
#define SUCCEED(x) (x)
#endif