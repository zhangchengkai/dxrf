#include "../Common/stdafx.h"
#include "../Common/SharedVarMap.h"


SharedVarMap::SharedVarMap(std::string name, bool b_is_creator) :_b_is_creator(b_is_creator)
{
	if (_b_is_creator)
	{
		_h_shared_mem_file = CreateFileA((name + ".sharedmem").c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		_h_shared_mem_obj = CreateFileMappingA(_h_shared_mem_file, NULL, PAGE_READWRITE, 0, MAX_MAP_SIZE, (name + "_filemapping").c_str());
		_h_shared_mem_mutex = CreateMutexA(NULL, FALSE, (name + "_filemapping_mutex").c_str());
	}
	else
	{
		_h_shared_mem_obj = OpenFileMappingA(FILE_MAP_ALL_ACCESS, FALSE, (name + "_filemapping").c_str());
		if (!_h_shared_mem_obj) { MessageBoxA(NULL, ("SharedVarMap: " + name + " has not been created yet!").c_str(), "Error", MB_OK); exit(1); }
		_h_shared_mem_mutex = OpenMutexA(SYNCHRONIZE, FALSE, (name + "_filemapping_mutex").c_str());
		if (!_h_shared_mem_mutex) { MessageBoxA(NULL, ("SharedVarMap: " + name + " has not been created yet!").c_str(), "Error", MB_OK); CloseHandle(_h_shared_mem_obj); exit(1); }
	}

	_p_map = reinterpret_cast<std::unordered_map<std::string, Value> *>(MapViewOfFile(_h_shared_mem_obj, FILE_MAP_ALL_ACCESS, 0, 0, MAX_MAP_SIZE));
}


SharedVarMap::~SharedVarMap()
{
	if(_b_is_creator)
		FlushViewOfFile(_p_map, MAX_MAP_SIZE);

	CloseHandle(_h_shared_mem_mutex);
	UnmapViewOfFile(_p_map);
	CloseHandle(_h_shared_mem_obj);

	if(_b_is_creator)
		CloseHandle(_h_shared_mem_file);
}
