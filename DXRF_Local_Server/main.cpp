#include "../Common/stdafx.h"
#include "../Common/SharedVarMap.h"


void update() {

}


int main() {
	HANDLE h_program_mutex = OpenMutexA(SYNCHRONIZE, FALSE, "DXRF_Local_Server_mutex");
	if (h_program_mutex) { MessageBoxA(NULL, "There is already an instance of DXRF Local Server running!", "Error", MB_OK); CloseHandle(h_program_mutex); exit(1); }
	h_program_mutex = CreateMutexA(NULL, TRUE, "DXRF_Local_Server_mutex");
	
	std::unique_ptr<SharedVarMap> p_svm = std::make_unique<SharedVarMap>("DXRF_SVM", false);
	
	long long update_interval = 15000000; // 15 ms
	while (true) {
		if (p_svm->get("b_should_exit", bool(0)))break;
		
		auto start = std::chrono::system_clock::now();
		update();
		auto end = std::chrono::system_clock::now();
		long long elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count(); // ns

		if (elapsed < update_interval)
			std::this_thread::sleep_for(std::chrono::nanoseconds(update_interval - elapsed));
	}

	CloseHandle(h_program_mutex);
	return 0;
}