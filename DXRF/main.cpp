#include "../Common/stdafx.h"
#include "D3dImpl.h"
using Microsoft::WRL::ComPtr;
using namespace DirectX;


HWND create_main_window(HINSTANCE hInstance, int clientWidth, int clientHeight);
LRESULT CALLBACK main_window_proc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);


int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, LPWSTR, int)
{
	// Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	int client_width = 800;
	int client_height = 600;

	HWND h_main_window = create_main_window(hInstance, client_width, client_height);
	std::unique_ptr<D3dImpl> d3d_impl = std::make_unique<D3dImpl>(client_width, client_height, h_main_window);

	MSG msg = { 0 };
	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			d3d_impl->calc_frame_rate();
			d3d_impl->draw();
		}
	}

	return 0;
}


HWND create_main_window(HINSTANCE hInstance, int client_width, int client_height)
{
	WNDCLASS wc;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = main_window_proc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(0, IDI_APPLICATION);
	wc.hCursor = LoadCursor(0, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(NULL_BRUSH);
	wc.lpszMenuName = 0;
	wc.lpszClassName = L"MainWnd";

	ASSERT(RegisterClass(&wc));

	// Compute window rectangle dimensions based on requested client area dimensions.
	RECT R = { 0, 0, client_width, client_height };
	AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);
	int width = R.right - R.left;
	int height = R.bottom - R.top;

	HWND h_main_window = CreateWindow(L"MainWnd", L"main window", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, width, height, 0, 0, hInstance, 0);
	ASSERT(h_main_window);
	
	ShowWindow(h_main_window, SW_SHOW);
	UpdateWindow(h_main_window);

	return h_main_window;
}


LRESULT CALLBACK main_window_proc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}
	
	return DefWindowProc(hwnd, msg, wParam, lParam);
}


