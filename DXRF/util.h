#pragma once
#include "../Common/stdafx.h"


Microsoft::WRL::ComPtr<ID3D12Resource> create_default_buffer(ID3D12Device * device, ID3D12GraphicsCommandList * cmdList, const void * initData, UINT64 byteSize, Microsoft::WRL::ComPtr<ID3D12Resource>& uploadBuffer);
Microsoft::WRL::ComPtr<ID3DBlob> load_cso(const std::wstring& filename);
UINT calc_cbuffer_size(UINT elementSize);
DirectX::XMFLOAT4X4 identity_4x4();