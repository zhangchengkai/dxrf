#include "../Common/stdafx.h"
#include "D3dImpl.h"
using namespace Microsoft::WRL;
using namespace DirectX;


D3dImpl::D3dImpl(int client_width, int client_height, HWND h_main_window)
{
	m_client_width = client_width;
	m_client_height = client_height;
	m_h_main_window = h_main_window;

#if defined(DEBUG) || defined(_DEBUG) 
	// Enable the D3D12 debug layer.
	ComPtr<ID3D12Debug> debug_controller;
	ASSERT(SUCCEED(D3D12GetDebugInterface(IID_PPV_ARGS(&debug_controller))));
	debug_controller->EnableDebugLayer();
#endif

	ASSERT(SUCCEED(CreateDXGIFactory1(IID_PPV_ARGS(&m_factory))));

	ASSERT(SUCCEED(D3D12CreateDevice(nullptr, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&m_device))));

	ASSERT(SUCCEED(m_device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&m_fence))));
	m_curr_fence_value = 0;

	m_rtv_descriptor_size = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	m_dsv_descriptor_size = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
	m_cbv_srv_uav_descriptor_size = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	create_command_objects();
	create_swap_chain();
	create_rtc_dsv_descriptor_heaps();
	create_additional_descriptor_heaps();

	create_view_port_and_scissor_rect();
	
	build_root_sig();
	build_shader_and_input_layout();
	build_geo();
	build_pso();

	fill_obj_cbuffer();

	ASSERT(SUCCEED(m_list->Close()));
	std::array<ID3D12CommandList*, 1> lists = { m_list.Get() };
	m_queue->ExecuteCommandLists(lists.size(), lists.data());

	flush_command_queue();
}


void D3dImpl::create_command_objects()
{
	D3D12_COMMAND_QUEUE_DESC queueDesc = {};
	queueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
	queueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	ASSERT(SUCCEED(m_device->CreateCommandQueue(&queueDesc, IID_PPV_ARGS(&m_queue))));

	ASSERT(SUCCEED(m_device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(m_alloc.GetAddressOf()))));

	ASSERT(SUCCEED(m_device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, m_alloc.Get(), nullptr, IID_PPV_ARGS(m_list.GetAddressOf()))));
}


void D3dImpl::create_swap_chain()
{
	DXGI_SWAP_CHAIN_DESC sd;
	sd.BufferDesc.Width = m_client_width;
	sd.BufferDesc.Height = m_client_height;
	sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	sd.SampleDesc.Count = 1;
	sd.SampleDesc.Quality = 0;
	sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	sd.BufferCount = 2;
	sd.OutputWindow = m_h_main_window;
	sd.Windowed = true;
	sd.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	sd.Flags = 0;

	ASSERT(SUCCEED(m_factory->CreateSwapChain(m_queue.Get(), &sd, m_swap_chain.GetAddressOf())));
}


void D3dImpl::create_rtc_dsv_descriptor_heaps()
{
	D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc;
	rtvHeapDesc.NumDescriptors = 2;
	rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	rtvHeapDesc.NodeMask = 0;
	ASSERT(SUCCEED(m_device->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(m_rtv_heap.GetAddressOf()))));

	D3D12_DESCRIPTOR_HEAP_DESC dsvHeapDesc;
	dsvHeapDesc.NumDescriptors = 1;
	dsvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	dsvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	dsvHeapDesc.NodeMask = 0;
	ASSERT(SUCCEED(m_device->CreateDescriptorHeap(&dsvHeapDesc, IID_PPV_ARGS(m_dsv_heap.GetAddressOf()))));

	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHeapHandle(m_rtv_heap->GetCPUDescriptorHandleForHeapStart());
	UINT rtvDescriptorSize = m_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	for (UINT i = 0; i < 2; i++)
	{
		ASSERT(SUCCEED(m_swap_chain->GetBuffer(i, IID_PPV_ARGS(&m_back_buffers[i]))));
		m_device->CreateRenderTargetView(m_back_buffers[i].Get(), nullptr, rtvHeapHandle);
		rtvHeapHandle.Offset(1, rtvDescriptorSize);
	}

	D3D12_RESOURCE_DESC depthStencilDesc;
	depthStencilDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	depthStencilDesc.Alignment = 0;
	depthStencilDesc.Width = m_client_width;
	depthStencilDesc.Height = m_client_height;
	depthStencilDesc.DepthOrArraySize = 1;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;
	depthStencilDesc.SampleDesc.Count = 1;
	depthStencilDesc.SampleDesc.Quality = 0;
	depthStencilDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	depthStencilDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	D3D12_CLEAR_VALUE optClear;
	optClear.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	optClear.DepthStencil.Depth = 1.0f;
	optClear.DepthStencil.Stencil = 0;

	ASSERT(SUCCEED(m_device->CreateCommittedResource(&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT), D3D12_HEAP_FLAG_NONE, &depthStencilDesc, D3D12_RESOURCE_STATE_DEPTH_WRITE, &optClear, IID_PPV_ARGS(m_main_ds_buffer.GetAddressOf()))));

	D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc;
	dsvDesc.Flags = D3D12_DSV_FLAG_NONE;
	dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	dsvDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	dsvDesc.Texture2D.MipSlice = 0;

	CD3DX12_CPU_DESCRIPTOR_HANDLE dsvHeapHandle(m_dsv_heap->GetCPUDescriptorHandleForHeapStart());
	m_device->CreateDepthStencilView(m_main_ds_buffer.Get(), &dsvDesc, dsvHeapHandle);
}


void D3dImpl::create_additional_descriptor_heaps()
{
	D3D12_DESCRIPTOR_HEAP_DESC cbvHeapDesc;
	cbvHeapDesc.NumDescriptors = 1;
	cbvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	cbvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	cbvHeapDesc.NodeMask = 0;

	ASSERT(SUCCEED(m_device->CreateDescriptorHeap(&cbvHeapDesc, IID_PPV_ARGS(&m_cbv_srv_uav_heap))));

	m_obj_cbuffer = std::make_unique<UploadBuffer<ObjectConstant>>(m_device.Get(), 1, true);

	D3D12_CONSTANT_BUFFER_VIEW_DESC cbvDesc;
	cbvDesc.BufferLocation = m_obj_cbuffer->resource()->GetGPUVirtualAddress();;
	cbvDesc.SizeInBytes = calc_cbuffer_size(sizeof(ObjectConstant));

	m_device->CreateConstantBufferView(&cbvDesc, m_cbv_srv_uav_heap->GetCPUDescriptorHandleForHeapStart());
}


void D3dImpl::flush_command_queue()
{
	ASSERT(SUCCEED(m_queue->Signal(m_fence.Get(), ++m_curr_fence_value)));
	if (m_fence->GetCompletedValue() < m_curr_fence_value)
	{
		HANDLE eventHandle = CreateEventEx(nullptr, false, false, EVENT_ALL_ACCESS);

		// Fire event when GPU hits current fence.  
		ASSERT(SUCCEED(m_fence->SetEventOnCompletion(m_curr_fence_value, eventHandle)));

		// Wait until the GPU hits current fence event is fired.
		WaitForSingleObject(eventHandle, INFINITE);
		CloseHandle(eventHandle);
	}
}


void D3dImpl::build_root_sig()
{
	CD3DX12_ROOT_PARAMETER rootParameterSlots[1];
	CD3DX12_DESCRIPTOR_RANGE cbvTable;
	cbvTable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0);
	rootParameterSlots[0].InitAsDescriptorTable(1, &cbvTable);

	CD3DX12_ROOT_SIGNATURE_DESC rootSigDesc(1, rootParameterSlots, 0, nullptr, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

	ComPtr<ID3DBlob> serializedRootSig;
	ASSERT(SUCCEED(D3D12SerializeRootSignature(&rootSigDesc, D3D_ROOT_SIGNATURE_VERSION_1, serializedRootSig.GetAddressOf(), nullptr)));

	ASSERT(SUCCEED(m_device->CreateRootSignature(0, serializedRootSig->GetBufferPointer(), serializedRootSig->GetBufferSize(), IID_PPV_ARGS(&m_root_sig))));
}


void D3dImpl::build_shader_and_input_layout()
{
	m_vs = load_cso(L"default_vs.cso");
	m_ps = load_cso(L"default_ps.cso");

	m_input_layout =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};
}


void D3dImpl::build_geo()
{
	std::array<Vertex, 8> vertices =
	{
		Vertex({ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT4(Colors::White) }),
		Vertex({ XMFLOAT3(-1.0f, +1.0f, -1.0f), XMFLOAT4(Colors::Black) }),
		Vertex({ XMFLOAT3(+1.0f, +1.0f, -1.0f), XMFLOAT4(Colors::Red) }),
		Vertex({ XMFLOAT3(+1.0f, -1.0f, -1.0f), XMFLOAT4(Colors::Green) }),
		Vertex({ XMFLOAT3(-1.0f, -1.0f, +1.0f), XMFLOAT4(Colors::Blue) }),
		Vertex({ XMFLOAT3(-1.0f, +1.0f, +1.0f), XMFLOAT4(Colors::Yellow) }),
		Vertex({ XMFLOAT3(+1.0f, +1.0f, +1.0f), XMFLOAT4(Colors::Cyan) }),
		Vertex({ XMFLOAT3(+1.0f, -1.0f, +1.0f), XMFLOAT4(Colors::Magenta) })
	};

	std::array<std::uint16_t, 36> indices =
	{
		// front face
		0, 1, 2,
		0, 2, 3,

		// back face
		4, 6, 5,
		4, 7, 6,

		// left face
		4, 5, 1,
		4, 1, 0,

		// right face
		3, 2, 6,
		3, 6, 7,

		// top face
		1, 5, 6,
		1, 6, 2,

		// bottom face
		4, 0, 3,
		4, 3, 7
	};

	const UINT vbByteSize = (UINT)vertices.size() * sizeof(Vertex);
	const UINT ibByteSize = (UINT)indices.size() * sizeof(std::uint16_t);

	m_geo = std::make_unique<MeshGeometry>();
	m_geo->Name = "boxGeo";

	ASSERT(SUCCEED(D3DCreateBlob(vbByteSize, &m_geo->VertexBufferCPU)));
	CopyMemory(m_geo->VertexBufferCPU->GetBufferPointer(), vertices.data(), vbByteSize);

	ASSERT(SUCCEED(D3DCreateBlob(ibByteSize, &m_geo->IndexBufferCPU)));
	CopyMemory(m_geo->IndexBufferCPU->GetBufferPointer(), indices.data(), ibByteSize);

	m_geo->VertexBufferGPU = create_default_buffer(m_device.Get(),
		m_list.Get(), vertices.data(), vbByteSize, m_geo->VertexBufferUploader);

	m_geo->IndexBufferGPU = create_default_buffer(m_device.Get(),
		m_list.Get(), indices.data(), ibByteSize, m_geo->IndexBufferUploader);

	m_geo->VertexByteStride = sizeof(Vertex);
	m_geo->VertexBufferByteSize = vbByteSize;
	m_geo->IndexFormat = DXGI_FORMAT_R16_UINT;
	m_geo->IndexBufferByteSize = ibByteSize;

	SubmeshGeometry submesh;
	submesh.IndexCount = (UINT)indices.size();
	submesh.StartIndexLocation = 0;
	submesh.BaseVertexLocation = 0;

	m_geo->DrawArgs["box"] = submesh;
}


void D3dImpl::build_pso()
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};
	psoDesc.InputLayout = { m_input_layout.data(), (UINT)m_input_layout.size() };
	psoDesc.pRootSignature = m_root_sig.Get();
	psoDesc.VS =
	{
		reinterpret_cast<BYTE*>(m_vs->GetBufferPointer()),
		m_vs->GetBufferSize()
	};
	psoDesc.PS =
	{
		reinterpret_cast<BYTE*>(m_ps->GetBufferPointer()),
		m_ps->GetBufferSize()
	};
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.SampleDesc.Count = 1;
	psoDesc.SampleDesc.Quality = 0;
	psoDesc.DSVFormat = DXGI_FORMAT_D24_UNORM_S8_UINT;
	ASSERT(SUCCEED(m_device->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_pso))));
}


void D3dImpl::fill_obj_cbuffer()
{
	float theta = 1.5f*XM_PI;
	float phi = XM_PIDIV4;
	float radius = 5.0f;

	// Convert Spherical to Cartesian coordinates.
	float x = radius * sinf(phi)*cosf(theta);
	float z = radius * sinf(phi)*sinf(theta);
	float y = radius * cosf(phi);

	// Build the view matrix.
	XMVECTOR pos = XMVectorSet(x, y, z, 1.0f);
	XMVECTOR target = XMVectorZero();
	XMVECTOR up = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);

	XMMATRIX view = XMMatrixLookAtLH(pos, target, up);

	XMMATRIX world = XMLoadFloat4x4(&identity_4x4());

	XMMATRIX proj = XMMatrixPerspectiveFovLH(XM_PIDIV4, m_aspect_ratio(), 1.0f, 1000.0f);
	
	XMMATRIX world_view_proj = world * view * proj;

	ObjectConstant obj_constant;
	XMStoreFloat4x4(&obj_constant.world_view_proj, XMMatrixTranspose(world_view_proj));
	m_obj_cbuffer->copy_data(0, obj_constant);
}


void D3dImpl::create_view_port_and_scissor_rect()
{
	m_view_port.TopLeftX = 0;
	m_view_port.TopLeftY = 0;
	m_view_port.Width = static_cast<float>(m_client_width);
	m_view_port.Height = static_cast<float>(m_client_height);
	m_view_port.MinDepth = 0.0f;
	m_view_port.MaxDepth = 1.0f;

	m_scissor_rect = { 0, 0, m_client_width, m_client_width };
}


void D3dImpl::draw()
{
	// Reuse the memory associated with command recording.
	// We can only reset when the associated command lists have finished execution on the GPU.
	ASSERT(SUCCEED(m_alloc->Reset()));

	// A command list can be reset after it has been added to the command queue via ExecuteCommandList.
	// Reusing the command list reuses memory.
	ASSERT(SUCCEED(m_list->Reset(m_alloc.Get(), m_pso.Get())));

	m_list->RSSetViewports(1, &m_view_port);
	m_list->RSSetScissorRects(1, &m_scissor_rect);

	// Indicate a state transition on the resource usage.
	m_list->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(m_curr_back_buffer(),
		D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));

	// Clear the back buffer and depth buffer.
	m_list->ClearRenderTargetView(m_curr_back_buffer_view(), Colors::LightSteelBlue, 0, nullptr);
	m_list->ClearDepthStencilView(m_main_ds_buffer_view(), D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, nullptr);

	// Specify the buffers we are going to render to.
	m_list->OMSetRenderTargets(1, &m_curr_back_buffer_view(), true, &m_main_ds_buffer_view());

	std::array<ID3D12DescriptorHeap*, 1> descriptorHeaps = { m_cbv_srv_uav_heap.Get() };
	m_list->SetDescriptorHeaps(descriptorHeaps.size(), descriptorHeaps.data());

	m_list->SetGraphicsRootSignature(m_root_sig.Get());

	m_list->IASetVertexBuffers(0, 1, &m_geo->VertexBufferView());
	m_list->IASetIndexBuffer(&m_geo->IndexBufferView());
	m_list->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	m_list->SetGraphicsRootDescriptorTable(0, m_cbv_srv_uav_heap->GetGPUDescriptorHandleForHeapStart());

	m_list->DrawIndexedInstanced(m_geo->DrawArgs["box"].IndexCount, 1, m_geo->DrawArgs["box"].StartIndexLocation, m_geo->DrawArgs["box"].BaseVertexLocation, 0);

	// Indicate a state transition on the resource usage.
	m_list->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(m_curr_back_buffer(),
		D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));

	// Done recording commands.
	ASSERT(SUCCEED(m_list->Close()));

	// Add the command list to the queue for execution.
	std::array<ID3D12CommandList*, 1> lists = { m_list.Get() };
	m_queue->ExecuteCommandLists(lists.size(), lists.data());

	// swap the back and front buffers
	ASSERT(SUCCEED(m_swap_chain->Present(1, 0)));
	m_curr_back_buffer_index = (m_curr_back_buffer_index + 1) % 2;

	// Wait until frame commands are complete.  This waiting is inefficient and is
	// done for simplicity.  Later we will show how to organize our rendering code
	// so we do not have to wait per frame.
	flush_command_queue();
}


void D3dImpl::calc_frame_rate()
{
	// init
	static int frameCnt = 0;
	static long long timeElapsed = 0; // ns
	static auto last_time = std::chrono::system_clock::now();

	frameCnt++;

	timeElapsed += std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now() - last_time).count();
	last_time = std::chrono::system_clock::now();

	if (timeElapsed >= 1000000000)
	{
		std::wstring fpsStr = std::to_wstring(frameCnt);
		std::wstring mspfStr = std::to_wstring(1000.0f / frameCnt);

		std::wstring windowText = L"DXRF    fps: " + fpsStr + L"    mspf: " + mspfStr;

		SetWindowText(m_h_main_window, windowText.c_str());

		frameCnt = 0;
		timeElapsed = 0;
	}
}