#pragma once
#include "../Common/stdafx.h"
#include "util.h"
#include "MeshGeometry.h"
#include "UploadBuffer.h"


struct ObjectConstant
{
	DirectX::XMFLOAT4X4 world_view_proj;
};


struct Vertex
{
	DirectX::XMFLOAT3 pos;
	DirectX::XMFLOAT4 color;
};


class D3dImpl
{
public:
	D3dImpl(int client_width, int client_height, HWND h_main_window);
	~D3dImpl() { flush_command_queue(); }
	void draw();
	void calc_frame_rate();
private:
	int m_client_width;
	int m_client_height;
	HWND m_h_main_window;
	float m_aspect_ratio()const { return static_cast<float>(m_client_width) / m_client_height; }

	Microsoft::WRL::ComPtr<IDXGIFactory4> m_factory;
	Microsoft::WRL::ComPtr<ID3D12Device> m_device;

	Microsoft::WRL::ComPtr<ID3D12Fence> m_fence;
	UINT64 m_curr_fence_value;
	void flush_command_queue();

	UINT m_rtv_descriptor_size;
	UINT m_dsv_descriptor_size;
	UINT m_cbv_srv_uav_descriptor_size;

	Microsoft::WRL::ComPtr<ID3D12CommandQueue> m_queue;
	Microsoft::WRL::ComPtr<ID3D12CommandAllocator> m_alloc;
	Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> m_list;
	void create_command_objects();

	Microsoft::WRL::ComPtr<IDXGISwapChain> m_swap_chain;
	void create_swap_chain();

	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> m_rtv_heap;
	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> m_dsv_heap;
	Microsoft::WRL::ComPtr<ID3D12Resource> m_back_buffers[2];
	Microsoft::WRL::ComPtr<ID3D12Resource> m_main_ds_buffer;
	int m_curr_back_buffer_index = 0;
	ID3D12Resource *m_curr_back_buffer() { return m_back_buffers[m_curr_back_buffer_index].Get(); }
	D3D12_CPU_DESCRIPTOR_HANDLE m_curr_back_buffer_view()const
	{
		return CD3DX12_CPU_DESCRIPTOR_HANDLE(
			m_rtv_heap->GetCPUDescriptorHandleForHeapStart(),
			m_curr_back_buffer_index,
			m_rtv_descriptor_size);
	}
	D3D12_CPU_DESCRIPTOR_HANDLE m_main_ds_buffer_view()const { return m_dsv_heap->GetCPUDescriptorHandleForHeapStart(); }
	void create_rtc_dsv_descriptor_heaps();

	Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> m_cbv_srv_uav_heap;
	std::unique_ptr<UploadBuffer<ObjectConstant>> m_obj_cbuffer;
	void create_additional_descriptor_heaps();

	D3D12_VIEWPORT m_view_port;
	D3D12_RECT m_scissor_rect;
	void create_view_port_and_scissor_rect();

	Microsoft::WRL::ComPtr<ID3D12RootSignature> m_root_sig;
	void build_root_sig();

	Microsoft::WRL::ComPtr<ID3DBlob> m_vs;
	Microsoft::WRL::ComPtr<ID3DBlob> m_ps;
	std::vector<D3D12_INPUT_ELEMENT_DESC> m_input_layout;
	void build_shader_and_input_layout();

	std::unique_ptr<MeshGeometry> m_geo;
	void build_geo();

	Microsoft::WRL::ComPtr<ID3D12PipelineState> m_pso;
	void build_pso();

	void fill_obj_cbuffer();

};

