#include "calc_max_data_size.h"
#include <algorithm>
#include <DirectXMath.h>
using namespace std;

size_t calc_max_data_size()
{
	size_t max_data_size = 0;

	max_data_size = max(max_data_size, sizeof(DirectX::XMMATRIX));

	return max_data_size;
}