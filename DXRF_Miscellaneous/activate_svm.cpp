#include "activate_svm.h"
#include "../Common/SharedVarMap.h"
#include <string>
#include <unordered_map>

void activate_svm(std::string name)
{
	SharedVarMap svm(name, true);
	ZeroMemory(svm._p_map, SharedVarMap::MAX_MAP_SIZE);
	new (svm._p_map) std::unordered_map<std::string, SharedVarMap::Value>();
}
