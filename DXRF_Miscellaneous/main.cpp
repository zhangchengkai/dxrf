#include <iostream>
#include "calc_max_data_size.h"
#include "activate_svm.h"
using namespace std;

int main()
{
	cout << "DXRF_Constant::MAX_DATA_SIZE	" << calc_max_data_size() << endl << endl;
	activate_svm("DXRF_SVM");
	return 0;
}