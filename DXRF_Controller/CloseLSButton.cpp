#include "../Common/stdafx.h"
#include "CloseLSButton.h"


CloseLSButton::CloseLSButton(HWND h_parent_window, std::wstring bt_txt, int x, int y, int client_width, int client_height, COLORREF color, COLORREF color_click, int font_size, HINSTANCE h_instance, MainWindow * main_wnd) :Button(h_parent_window, bt_txt, x, y, client_width, client_height, color, color_click, font_size, h_instance), main_wnd(main_wnd) {}


CloseLSButton::~CloseLSButton() {}


LRESULT CloseLSButton::proc_lcl(UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
	case WM_PAINT: {
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(h_button, &ps);
		if (b_is_clicked_down) FillRect(hdc, &ps.rcPaint, bg_click_brush);
		else FillRect(hdc, &ps.rcPaint, bg_brush);
		SetBkMode(hdc, TRANSPARENT);
		SelectObject(hdc, bt_font);
		DrawText(hdc, bt_txt.c_str(), -1, &ps.rcPaint, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
		EndPaint(h_button, &ps);
		return 0;
	}
	case WM_LBUTTONDOWN: {
		b_is_clicked_down = TRUE;
		InvalidateRect(h_button, NULL, TRUE);
		return 0;
	}
	case WM_LBUTTONUP: {
		b_is_clicked_down = FALSE;
		InvalidateRect(h_button, NULL, TRUE);
		main_wnd->p_svm->set("b_should_exit", true);
		return 0;
	}
	}

	return DefWindowProc(h_button, message, wParam, lParam);
}
