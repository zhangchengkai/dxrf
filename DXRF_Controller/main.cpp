#include "../Common/stdafx.h"
#include <Windows.h>
#include <string>
#include "MainWindow.h"
using namespace std;


int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, LPWSTR, int) {
	HANDLE h_program_mutex = OpenMutexA(SYNCHRONIZE, FALSE, "DXRF_Controller_mutex");
	if (h_program_mutex) { MessageBoxA(NULL, "There is already an instance of DXRF Controller running!", "Error", MB_OK); CloseHandle(h_program_mutex); exit(1); }
	h_program_mutex = CreateMutexA(NULL, TRUE, "DXRF_Controller_mutex");
	
	int client_width = 1280;
	int client_height = 720;
	MainWindow main_wnd(client_width, client_height, RGB(0, 0, 0), L"DXRF Controller", hInstance);

	MSG msg = { 0 };
	while (msg.message != WM_QUIT) {
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	CloseHandle(h_program_mutex);
	return 0;
}

