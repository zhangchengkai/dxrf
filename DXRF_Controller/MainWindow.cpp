#include "../Common/stdafx.h"
#include "MainWindow.h"
#include "StartLSButton.h"
#include "CloseLSButton.h"


MainWindow::MainWindow(int client_width, int client_height, COLORREF color, std::wstring caption, HINSTANCE hInstance) :client_width(client_width), client_height(client_height), color(color), caption(caption), hInstance(hInstance)
{
	WNDCLASS test_wc;
	if (GetClassInfo(hInstance, L"MainWindow_cls", &test_wc)) {
		MessageBox(NULL, L"There is already an instance of MainWindow exist!", L"Error", MB_OK);
		exit(1);
	}

	bg_brush = CreateSolidBrush(color);
	WNDCLASS wc;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = MainWindow::proc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(0, IDI_APPLICATION);
	wc.hCursor = LoadCursor(0, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wc.lpszMenuName = 0;
	wc.lpszClassName = L"MainWindow_cls";
	RegisterClass(&wc);

	// Compute window rectangle dimensions based on requested client area dimensions.
	RECT R = { 0, 0, client_width, client_height };
	AdjustWindowRect(&R, WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX, false);
	int width = R.right - R.left;
	int height = R.bottom - R.top;

	h_window = CreateWindow(L"MainWindow_cls", caption.c_str(), WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX, CW_USEDEFAULT, CW_USEDEFAULT, width, height, 0, 0, hInstance, 0);
	ShowWindow(h_window, SW_SHOW);
	UpdateWindow(h_window);

	p_bt_start_ls = std::make_unique<StartLSButton>(h_window, L"Start Local Server", client_width / 20, client_height / 20, client_width * 17 / 40, client_height * 17 / 40, RGB(100, 200, 100), RGB(50, 150, 50), client_height / 10, hInstance, this);
	p_bt_close_ls = std::make_unique<CloseLSButton>(h_window, L"Close Local Server", client_width / 20, client_height * 21 / 40, client_width * 17 / 40, client_height * 17 / 40, RGB(200, 100, 100), RGB(150, 50, 50), client_height / 10, hInstance, this);
	p_svm = std::make_unique<SharedVarMap>("DXRF_SVM", true);
}


HBRUSH MainWindow::bg_brush;


LRESULT CALLBACK MainWindow::proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
	case WM_CLOSE:
		if (MessageBox(hWnd, L"Do you want to quit?", L"Quit App", MB_YESNOCANCEL) == IDYES)
			DestroyWindow(hWnd);
		return 0;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	case WM_PAINT:
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		FillRect(hdc, &ps.rcPaint, bg_brush);
		EndPaint(hWnd, &ps);
		return 0;
	}
	return DefWindowProc(hWnd, message, wParam, lParam);
}


MainWindow::~MainWindow()
{
	DeleteObject(bg_brush);
}
