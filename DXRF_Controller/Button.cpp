#include "../Common/stdafx.h"
#include "Button.h"
#include <sstream>

Button::Button(HWND h_parent_window, std::wstring bt_txt, int x, int y, int cilent_width, int client_height, COLORREF color, COLORREF color_click, int font_size, HINSTANCE h_instance) :h_parent_window(h_parent_window), bt_txt(bt_txt), x(x), y(y), client_width(cilent_width), client_height(client_height), color(color), color_click(color_click), font_size(font_size), h_instance(h_instance) {
	bg_brush = CreateSolidBrush(color);
	bg_click_brush = CreateSolidBrush(color_click);
	bt_font = CreateFont(font_size, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Arial");

	b_is_clicked_down = FALSE;

	WNDCLASS test_wc;
	if (!GetClassInfo(h_instance, L"Button_cls", &test_wc)) {
		WNDCLASS wc = { 0 };
		wc.style = CS_HREDRAW | CS_VREDRAW;
		wc.lpfnWndProc = (WNDPROC)(Button::proc);
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = h_instance;
		wc.hIcon = NULL;
		wc.hCursor = LoadCursor(0, IDC_HAND);
		wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wc.lpszMenuName = 0;
		wc.lpszClassName = L"Button_cls";

		RegisterClass(&wc);
	}

	RECT R = rc_client_area = { 0, 0, cilent_width, client_height };
	AdjustWindowRect(&R, WS_BORDER | WS_CHILD, false);
	int width = R.right - R.left;
	int height = R.bottom - R.top;

	h_button = CreateWindow(L"Button_cls", L"Button", WS_BORDER | WS_CHILD, x, y, width, height, h_parent_window, 0, h_instance, this);

	ShowWindow(h_button, SW_SHOW);
	UpdateWindow(h_button);
}


LRESULT Button::proc_lcl(UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
	case WM_PAINT: {
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(h_button, &ps);
		if (b_is_clicked_down) FillRect(hdc, &ps.rcPaint, bg_click_brush);
		else FillRect(hdc, &ps.rcPaint, bg_brush);
		SetBkMode(hdc, TRANSPARENT);
		SelectObject(hdc, bt_font);
		DrawText(hdc, bt_txt.c_str(), -1, &ps.rcPaint, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
		EndPaint(h_button, &ps);
		return 0;
	}
	case WM_LBUTTONDOWN: {
		b_is_clicked_down = TRUE;
		InvalidateRect(h_button, NULL, TRUE);
		return 0;
	}
	case WM_LBUTTONUP: {
		b_is_clicked_down = FALSE;
		InvalidateRect(h_button, NULL, TRUE);
		return 0;
	}
	}

	return DefWindowProc(h_button, message, wParam, lParam);
}


LRESULT CALLBACK Button::proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam) {
	Button *this_obj;

	if (message == WM_NCCREATE) {
		CREATESTRUCT *cs = (CREATESTRUCT*)lParam;
		this_obj = (Button *)cs->lpCreateParams;
		SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)this_obj);
		return DefWindowProc(hWnd, message, wParam, lParam); // 注意：此时构造函数中的CreateWindow还没有执行完，（如果WM_NCCREATE返回false，则window不会被创建），所以如果执行this_obj->proc_lcl则this_obj的h_button还没有被赋值，导致1400错误（ERROR_INVALID_WINDOW_HANDLE），而其他message一定是在Button的构造函数执行完后才会收到，因此可以正常使用this_obj->proc_lcl处理
	}
	else
		this_obj = (Button *)GetWindowLongPtr(hWnd, GWLP_USERDATA);
	
	return this_obj->proc_lcl(message, wParam, lParam);
}


Button::~Button() {
	DeleteObject(bg_brush);
	DeleteObject(bg_click_brush);
	DeleteObject(bt_font);
}
