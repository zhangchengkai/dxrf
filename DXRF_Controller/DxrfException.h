#pragma once
#include <exception>
#include <string>


class DxrfException :public std::exception
{
public:
	DxrfException(std::string error_str);
	const char * what() const throw () { return error_str.c_str(); }
private:
	std::string error_str;
};

