#pragma once
#include <Windows.h>
#include <string>
#include "Button.h"
#include <memory>
#include "../Common/SharedVarMap.h"


class MainWindow
{
public:
	MainWindow(int client_width, int client_height, COLORREF color, std::wstring caption, HINSTANCE hInstance);
	~MainWindow();

	std::unique_ptr<SharedVarMap> p_svm;
private:
	static LRESULT CALLBACK proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

	int client_width;
	int client_height;
	COLORREF color;
	std::wstring caption;
	HINSTANCE hInstance;

	static HBRUSH bg_brush;
	HWND h_window;
	std::unique_ptr<Button> p_bt_start_ls;
	std::unique_ptr<Button> p_bt_close_ls;
};

