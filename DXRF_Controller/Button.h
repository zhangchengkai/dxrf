#pragma once
#include <Windows.h>
#include <string>
class Button
{
public:
	Button(HWND h_parent_window, std::wstring bt_txt, int x, int y, int client_width, int client_height, COLORREF color, COLORREF color_click, int font_size, HINSTANCE h_instance);
	virtual ~Button();
protected:
	static LRESULT CALLBACK proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	virtual LRESULT proc_lcl(UINT message, WPARAM wParam, LPARAM lParam);
	
	HWND h_parent_window;
	std::wstring bt_txt;
	int x;
	int y;
	int client_width;
	int client_height;
	COLORREF color;
	COLORREF color_click;
	int font_size;
	HINSTANCE h_instance;

	HBRUSH bg_brush;
	HBRUSH bg_click_brush;
	HFONT bt_font;
	HWND h_button;
	RECT rc_client_area;
	BOOL b_is_clicked_down;
};

