#pragma once
#include "Button.h"
#include "MainWindow.h"

class StartLSButton :public Button
{
public:
	StartLSButton(HWND h_parent_window, std::wstring bt_txt, int x, int y, int client_width, int client_height, COLORREF color, COLORREF color_click, int font_size, HINSTANCE h_instance, MainWindow * main_wnd);
	virtual ~StartLSButton() override;
protected:
	virtual LRESULT proc_lcl(UINT message, WPARAM wParam, LPARAM lParam) override;

	MainWindow *main_wnd;
};

